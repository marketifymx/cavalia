$(document).ready(function (e){
	
	$("#contact").on('submit',(function(e){
	e.preventDefault();
	var valid;
	valid = validateContact();
	if(valid) {
		$.ajax({
		url: "php/contact.php",
		type: "POST",
		data:  new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
			$("#contact").hide();
			$("#resultado").show();
			$("#resultado").html(data);
		},
		error: function(){}
		});
	}
}));

function validateContact() {
	var valid = true;
	$(".form-control").css('background-color','');
	$(".mail-info").html('');
	
	if(!$("#nombre").val()) {
		$("#nombre-info").html("(required)");
		$("#nombre").css('border','red 1px solid');
		valid = false;
	}
	if(!$("#email").val()) {
		$("#email-info").html("(required)");
		$("#email").css('border','red 1px solid');
		valid = false;
	}
	if(!$("#email").val().match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/)) {
		$("#email-info").html("(invalid)");
		$("#email").css('border','red 1px solid');
		$("#correo").text("Por favor, ingrese un correo válido.");
		$("#email").val('');
		valid = false;
	}
	if(!$("#tel").val()) {
		$("#tel-info").html("(required)");
		$("#tel").css('border','red 1px solid');
		valid = false;
	}
	return valid;
}

});

var msg="";
	var elements = document.getElementsByTagName("INPUT");
	
	for (var i = 0; i < elements.length; i++) {
	   elements[i].oninvalid =function(e) {
	        if (!e.target.validity.valid) {
	        switch(e.target.id){
	            case 'popnombre' : 
	            e.target.setCustomValidity("Ingresa tu nombre.");break;
	            case 'popempresa' : 
	            e.target.setCustomValidity("Nombre de la empresa.");break;
	            case 'poptel' : 
	            e.target.setCustomValidity("Teléfono 10 dígitos.");break;
	            case 'popemail' : 
	            e.target.setCustomValidity("Ingresa un email válido.");break;
	            case 'poppuesto' : 
	            e.target.setCustomValidity("Ingresa tu puesto.");break;
	        default : e.target.setCustomValidity("");break;
	
	        }
	       }
	    };
	   elements[i].oninput = function(e) {
	        e.target.setCustomValidity(msg);
	    };
	}

function isNumberKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
	return true;
}

function initMap() {
    
    var address = {lat: 20.703403, lng: -103.370720};
    
    var map = new google.maps.Map(document.getElementById('map'), {
        center: address,
        scrollwheel: false,
        zoom: 16
    });
    
    var marker = new google.maps.Marker({
	    position: address,
	    title:"Cavalia"
	});
	// To add the marker to the map, call setMap();
	marker.setMap(map);
}
