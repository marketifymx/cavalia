$(function(){
    //Boton en index.php
    $(".btnBuscar").on('click', function(e){
        e.preventDefault();
        var categoria = $("#categoria").val();
        if(categoria == null){
            categoria = "cualquiera";
        }
        var precio = $("#precio").val();
        if(precio == null){
            precio = "cualquiera";
        }
        var tipo = $("#tipo").val();
        if(tipo == null){
            tipo = "cualquiera";
        }

        var url="busqueda.php?categoria="+categoria+"&tipo="+tipo+"&precio="+precio;

        var pisos = $("#pisos").val();
        if(pisos != null && pisos != 'cualquiera'){
            url += "&pisos="+pisos;
        }

        var banos = $("#banos").val();
        if(banos != null && banos != 'cualquiera'){
            url += "&banos="+banos;
        }

        var recamaras = $("#recamaras").val();
        if(recamaras != null && recamaras != 'cualquiera'){
            url += "&recamaras="+recamaras;
        }

        window.open(url,"_self");
    });

    //Boton en busqueda.php
	$("#btnBuscar").on('click', function(e){
		e.preventDefault();
		var srt = $("#formBusqueda").serialize();
		$.ajax({
            beforeSend: function(){
                //alertify.log("Espera...");
                $(".contenido").html('<h3 class="encabezado"><i class="fa fa-spinner fa-spin fa-fw"></i><span class="sr-only">Espera...</span> Cargando contenido...</h3>');
            },
            type:'POST',
            cache:'false',
            dataType:'JSON',
            url:'admin/api/getInmuebles.php',
            data:srt+"&action=list",
            dataType:'json',
            success:function(response){
	        	if(response.Result == true){
	        		$(".contenido").html(response.Contenido);
	        		$('.flexslider').flexslider({
					    animation: "slide"
					  });
                    $(".fancybox").fancybox();
	        	}
            },
            error:function(){
                alert("Error general del sistema. Intente más tarde.");
            }
        });
	});

	$("#btnBuscar").click();

});