<?php 
  session_start();
  //Comprueba la sesion
  if(!isset($_SESSION['usr_id'])){
     header("location:login.html");
  }
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cavalia - Backend</title>
	<meta name="description" content="">
	<meta name="author" content="Danny Garcia">
	<link rel="stylesheet" href="css/bootstrap/bootstrap.css" /> 
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/app.v1.css" />
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>	
	<!-- Loader -->
    <div class="loading-container">
      <div class="loading">
        <div class="l1">
          <div></div>
        </div>
        <div class="l2">
          <div></div>
        </div>
        <div class="l3">
          <div></div>
        </div>
        <div class="l4">
          <div></div>
        </div>
      </div>
    </div>
	<aside class="left-panel">
            <div class="user text-center">
                  <img src="uploads/<?php echo $_SESSION['usr_img'];?>" class="img-circle" alt="...">
                  <h4 class="user-name"><?php echo $_SESSION['usr_nombre'];?></h4>
                  
                  <div class="dropdown user-login">
                  <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-circle status-icon available"></i> Disponible <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" href="inc/logout.php"><i class="fa fa-circle status-icon signout"></i> Salir</a></li>
                  </ul>
                  </div>	 
            </div>
            
            <nav class="navigation">
            	<ul class="list-unstyled">
                  <li><a href="inmuebles.php"><i class="fa fa-home"></i><span class="nav-label">Inmuebles</span></a></li>
                  <li><a href="inc/logout.php"><i class="fa fa-power-off"></i><span class="nav-label">Salir</span></a></li>
                </ul>
            </nav>
            
    </aside>
    
    <section class="content">
    	
        <header class="top-head container-fluid">
            <button type="button" class="navbar-toggle pull-left">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <form role="search" class="navbar-left app-search pull-left hidden-xs">
              <input type="text" placeholder="Buscar..." class="form-control form-control-circle">
         	</form>
            
            <nav class=" navbar-default hidden-xs" role="navigation">
                <ul class="nav navbar-nav">
                <li><a href="nuevo.php">Nuevo inmueble</a></li>
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">Ajustes<span class="caret"></span></a>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="perfil.php">Perfil</a></li>
                    <li><a href="inc/logout.php">Salir</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
            
            <ul class="nav-toolbar">

            </ul>
        </header>
        <div class="warper container-fluid">
          <div class="page-header"><h1>Cavalia <small>Administración</small></h1></div>
                 
        </div>
        <footer class="container-fluid footer">
        	Copyright &copy; 2017 <a href="http://cavalia.com/" target="_blank">CAVALIA</a>
            <a href="#" class="pull-right scrollToTop"><i class="fa fa-chevron-up"></i></a>
        </footer>
        
    
    </section>

	 <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="js/underscore/underscore-min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <script src="js/globalize/globalize.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
	   <script src="js/custom.js" type="text/javascript"></script>
</body>
</html>