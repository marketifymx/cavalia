$(function(){
	var myApp;
	myApp = myApp || (function () {var pleaseWaitDiv = $('<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h4 class="modal-title">ESPERA POR FAVOR...</h4></div><div class="modal-body"><div class="has-switch" tabindex="0"><div class="progress progress-striped active progress-sm"><div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% Complete</span></div></div></div></div></div></div></div>');return {showPleaseWait: function() {pleaseWaitDiv.modal('show');},hidePleaseWait: function () {pleaseWaitDiv.modal('hide');},};})();
	
	$('#formularioSesion').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nickname: {
                message: 'Este campo no puede estar vacio',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacio'
                    }
                }
            },
            password: {
                message: 'Este campo no puede estar vacio',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacio'
                    }
                }
            }
        }
    });
	
	$("#formularioSesion").on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');  
        var srt = $form.serialize();
        $.ajax({
			beforeSend: function(){
				myApp.showPleaseWait();
			},
			cache: false,
			type: "POST",
			dataType: "json",
			url:"inc/login.php",
			data:srt+"&accion=sesion",
			success: function(response){
				if(response.Respuesta == false){
					alertify.error("Error al iniciar sesión, revisa tus datos");
					myApp.hidePleaseWait();
				}else{
					window.open("inmuebles.php","_self");
            		myApp.hidePleaseWait();
            	}
			},
			error:function(){
				alertify.error('ERROR GENERAL DEL SISTEMA, INTENTE MAS TARDE');
				myApp.hidePleaseWait();
			}
		});
    });
	
	$("#btnRecuperar").on('click',function(e){
		e.preventDefault();
		var email = $("#email").val();

		$.ajax({beforeSend: function(){
			alertify.log("Espera...");
			},
			cache: false,
			type: "POST",
			dataType: "json",
			url:"inc/recuperar.php",
			data:"accion=recuperar&email="+email,
			success: function(response){
				if(response.Respuesta == false){
					alertify.error(response.Mensaje);
				}else{
					alertify.success("Se han enviado los datos de acceso a tu email.");
					$("#btnCancelar").click();
				}
			},
			error:function(){
				alertify.error('ERROR GENERAL DEL SISTEMA, INTENTE MAS TARDE');
			}
		});
	});
});