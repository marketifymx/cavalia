$(function(){
    var myApp;
    myApp = myApp || (function () {var pleaseWaitDiv = $('<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h4 class="modal-title">ESPERA POR FAVOR...</h4></div><div class="modal-body"><div class="has-switch" tabindex="0"><div class="progress progress-striped active progress-sm"><div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% Complete</span></div></div></div></div></div></div></div>');return {showPleaseWait: function() {pleaseWaitDiv.modal('show');},hidePleaseWait: function () {pleaseWaitDiv.modal('hide');},};})();

    $('#formUser').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nickname: {
                message: 'Este campo no puede estar vacio',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacio'
                    }
                }
            },
            nombre: {
                message: 'Este campo no puede estar vacio',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacio'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacio'
                    },
                    emailAddress:{
                        message: 'Ingrese una cuenta de correo valida'
                    }
                }
            },
            password: {
                validators: {
                    different: {
                        field: 'nickname',
                        message: 'La contraseña no puede ser igual al nombre de usuario'
                    }
                }
            }
        }
    });
    
    $("#formUser").on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');  
        var srt = $form.serialize();
        $.ajax({
            beforeSend: function(){
                myApp.showPleaseWait();
            },
            cache: false,
            type: "POST",
            dataType: "json",
            url:"inc/ControladorUsuario.php",
            data:srt+"&action=update",
            success: function(response){
                if(response.Result == false){
                    alertify.error("Error al iniciar sesión, revisa tus datos");
                    myApp.hidePleaseWait();
                }else{
                    myApp.hidePleaseWait();
                    alertify.success("Se han guardado tus cambios");
                }
            },
            error:function(){
                alertify.error('ERROR GENERAL DEL SISTEMA, INTENTE MAS TARDE');
                myApp.hidePleaseWait();
            }
        });
    });

    //Subir imagen de perfil
    upload = new AjaxUpload('addImage', {
        action: 'inc/uploadImage.php',
        onSubmit: function(file,ext){
            if (! (ext && /^(jpg|png)$/.test(ext))){
                // extensiones permitidas
                alertify.error('Sólo se permiten Imagenes .jpg o .png');
                return false;
             } else {
                $("#contenedorImg").html('<img src="uploads/loading.gif" id="addImage" width="150px" height="150px" />');
                this.disable();
             }
        },
        onComplete: function(file, response){                        
            this.enable();
            var respuesta = jQuery.parseJSON(response);
            if(respuesta.respuesta == 'done'){
                $("#contenedorImg").html('<img src="uploads/' + respuesta.fileName + '" id="addImage" width="150px" height="150px"  id="addImage" />');
                $("#imagen").val(respuesta.fileName);
            }else{
                alertify.error(respuesta.mensaje);
            }

            this.enable();
        }
    });
	/*$("#form").validationEngine();

	$("#btnGuardar").on('click', function(){
		var res = $("#form").validationEngine('validate');
        if(res == true){
        	var srt = $("#form").serialize();
        	var idusuario = $("#usuario").val();
        	$.ajax({
                beforeSend: function(){
                    alertify.log("Espera...");
                },
                type:'POST',
                cache:'false',
                dataType:'JSON',
                url:'inc/ControladorUsuario2.php',
                data:srt+"&action=update&idusuario="+idusuario,
                dataType:'json',
                success:function(response){
                    if(response.Result != true){
                        alertify.error("Ha ocurrido un error, revisa tus datos.");
                    }else{
                        alertify.success("Tus datos se han guardado correctamente.");
                    }
                },
                error:function(){
                    alertify.error("Error general del sistema. Intente más tarde.");
                }
            });
        }
	});*/	
});