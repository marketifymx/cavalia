$(function(){
	var myApp;
	myApp = myApp || (function () {var pleaseWaitDiv = $('<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button><h4 class="modal-title">ESPERA POR FAVOR...</h4></div><div class="modal-body"><div class="has-switch" tabindex="0"><div class="progress progress-striped active progress-sm"><div class="progress-bar progress-bar-success"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span class="sr-only">100% Complete</span></div></div></div></div></div></div></div>');return {showPleaseWait: function() {pleaseWaitDiv.modal('show');},hidePleaseWait: function () {pleaseWaitDiv.modal('hide');},};})();
	
	var arreglo = [];
	Dropzone.autoDiscover = false;
	
	var inmueble = $("#idinmueble").val();
	
	$(".dropzone").dropzone({
		acceptedFiles: "image/*",
		url: 'inc/uploadImage1.php',
		maxFiles: 5, // Number of files at a time
		maxFilesize: 2, //in MB
        thumbnailWidth: 250,
        thumbnailHeight: 250,    //
		dataType : 'json',
        init: function() {
            thisDropzone = this;
            $.post('inc/ControladorImagen.php?inmueble='+inmueble+"&accion=list", function(data) {
                var obj = jQuery.parseJSON(data);
                $.each(obj, function(key,value){
                    var mockFile = { name: value.name, size: value.size };                   
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);   
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "uploads/"+value.name);   
                });
                 
            });
        },
		maxfilesexceeded: function(file)
		{
			alert('¡Solo puedes subir 5 imágenes al mismo tiempo!');
		},
		success: function (file,response) {
			//subirImagen(response.fileName);
			console.log(response);
			var obj = jQuery.parseJSON( response);
			console.log(obj.fileName);
			arreglo.push(obj.fileName);
            guardarImagen(obj.fileName);
		},
		addRemoveLinks: true,
		removedfile: function(file) {
			console.log("Eliminado:" + file.name);
            eliminarImagen(file.name);
			var _ref; // Remove file on clicking the 'Remove file' button
			return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;

		}
	});
    
    
    function eliminarImagen(imagen){
        $.ajax({
            beforeSend: function(){

            },
            cache: false,
            type: "POST",
            dataType: "json",
            url:"inc/ControladorImagen1.php",
            data:"accion=delete&imagen="+imagen+"&inmueble="+ inmueble,
            success: function(response){
                if(response.Result == false){
                    alertify.error("Error al añadir imagen");
                }
            },
            error:function(){
                alertify.error('ERROR GENERAL DEL SISTEMA, INTENTE MAS TARDE');
            }
        });
    }

    function guardarImagen(imagen){
        $.ajax({
            beforeSend: function(){

            },
            cache: false,
            type: "POST",
            dataType: "json",
            url:"inc/ControladorImagen1.php",
            data:"accion=nuevo&imagen="+imagen+"&inmueble="+ inmueble,
            success: function(response){
                if(response.Result == false){
                    alertify.error("Error al añadir imagen");
                }
            },
            error:function(){
                alertify.error('ERROR GENERAL DEL SISTEMA, INTENTE MAS TARDE');
            }
        });
    }
    
	$('#formInmueble').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        button: {
	        selector: '#btnGuardar',
	        disabled: 'disabled'
	    },
        fields: {
            titulo: {
                message: 'Este campo no puede estar vacío',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacío'
                    }
                }
            },
            nivel: {
                message: 'Este campo no puede estar vacío',
                validators: {
                    notEmpty: {
                        message: 'Seleccione una opción'
                    },
                    numeric: {
                        message: 'Ingrese un número válido'
                    }
                }
            },
            tipo: {
                message: 'Seleccione una opción',
                validators: {
                    notEmpty: {
                        message: 'Seleccione una opción'
                    }
                }
            },
            estado: {
                message: 'Seleccione una opción',
                validators: {
                    notEmpty: {
                        message: 'Seleccione una opción'
                    }
                }
            },
            medidas: {
                message: 'Este campo no puede estar vacío',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacío'
                    }
                }
            },
            terraza: {
                message: 'Seleccione una opción',
                validators: {
                    notEmpty: {
                        message: 'Seleccione una opción'
                    }
                }
            },
            aservicio: {
                message: 'Seleccione una opción',
                validators: {
                    notEmpty: {
                        message: 'Seleccione una opción'
                    }
                }
            },
            cservicio: {
                message: 'Seleccione una opción',
                validators: {
                    notEmpty: {
                        message: 'Seleccione una opción'
                    }
                }
            },
            recamaras: {
                message: 'Este campo no puede estar vacío',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacío'
                    },
                    numeric: {
                        message: 'Ingrese un número válido'
                    }
                }
            },
            banios: {
                message: 'Este campo no puede estar vacío',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacío'
                    },
                    numeric: {
                        message: 'Ingrese un número válido'
                    }
                }
            },
            estacionamiento: {
                message: 'Este campo no puede estar vacío',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacío'
                    },
                    numeric: {
                        message: 'Ingrese un número válido'
                    }
                }
            },
            antiguedad: {
                message: 'Este campo no puede estar vacío',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacío'
                    }
                }
            },
            precio: {
                message: 'Este campo no puede estar vacío',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacio'
                    },
                    numeric: {
                        message: 'Ingrese un número válido'
                    }
                }
            },
            direccion: {
                message: 'Este campo no puede estar vacío',
                validators: {
                    notEmpty: {
                        message: 'Este campo no puede estar vacío'
                    }
                }
            },
        }
        
    });

	$("#btnGuardar").on('click', function(e){
        e.preventDefault();
		$("#formInmueble").bootstrapValidator('validate').on('success.form.bv', function(e){
			e.preventDefault();
	        var $form = $(e.target);
	        var bv = $form.data('bootstrapValidator');  
	        var srt = $form.serialize();
	        $.ajax({
                beforeSend: function(){
                    myApp.showPleaseWait();
                },
                cache: false,
                type: "POST",
                dataType: "json",
                url:"inc/ControladorInmueble.php",
                data:srt+"&accion=editar",
                success: function(response){
                    if(response.Result == false){
                        alert("Error al agregar un inmueble");
                        myApp.hidePleaseWait();
                    }else{
                        window.open("index.php","_self");
                        myApp.hidePleaseWait();
                    }
                },
                error:function(){
                    alert('ERROR GENERAL DEL SISTEMA, INTENTE MAS TARDE');
                    myApp.hidePleaseWait();
                }
            })
		});
	});
	
	//Funcion para eliminar elemento
	function removeA(arr){
		var what, a= arguments, L= a.length, ax;
		while(L> 1 && arr.length){
		    what= a[--L];
		    while((ax= arr.indexOf(what))!= -1){
		        arr.splice(ax, 1);
		    }
		}
		return arr;
	}
	/*
	var ary= ['three','seven','eleven'];
	removeA(ary,'seven')*/
	
});