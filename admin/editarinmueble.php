<?php 
  session_start();
  //Comprueba la sesion
  if(!isset($_SESSION['usr_id'])){
     header("location:login.html");
  }

  if(!isset($_GET['idinmueble'])){
     header("location:index.php");  
  }
  include("inc/config.php");
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cavalia - Backend</title>
	<meta name="description" content="">
	<meta name="author" content="Danny Garcia">
	<link rel="stylesheet" href="css/bootstrap/bootstrap.css" /> 
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.3/css/bootstrapValidator.min.css"/>
  <link href="js/alertify/themes/alertify.core.css" rel="stylesheet">
  <link href="js/alertify/themes/alertify.default.css" rel="stylesheet">
  <link href="js/dropzone/dropzone.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/app.v1.css" />
  <link rel="stylesheet" href="css/style.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>	
	<!-- Loader -->
    <div class="loading-container">
      <div class="loading">
        <div class="l1">
          <div></div>
        </div>
        <div class="l2">
          <div></div>
        </div>
        <div class="l3">
          <div></div>
        </div>
        <div class="l4">
          <div></div>
        </div>
      </div>
    </div>
	<aside class="left-panel">
            <div class="user text-center">
                  <img src="uploads/<?php echo $_SESSION['usr_img'];?>" class="img-circle" alt="...">
                  <h4 class="user-name"><?php echo $_SESSION['usr_nombre'];?></h4>
                  
                  <div class="dropdown user-login">
                  <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-circle status-icon available"></i> Disponible <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" href="inc/logout.php"><i class="fa fa-circle status-icon signout"></i> Salir</a></li>
                  </ul>
                  </div>	 
            </div>
            
            <nav class="navigation">
            	<ul class="list-unstyled">
                  <li><a href="inmuebles.php"><i class="fa fa-home"></i><span class="nav-label">Inmuebles</span></a></li>
                  <li><a href="inc/logout.php"><i class="fa fa-power-off"></i><span class="nav-label">Salir</span></a></li>
                </ul>
            </nav>
            
    </aside>
    
    <section class="content">
    	
        <header class="top-head container-fluid">
            <button type="button" class="navbar-toggle pull-left">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <form role="search" class="navbar-left app-search pull-left hidden-xs">
              <input type="text" placeholder="Buscar..." class="form-control form-control-circle">
         	</form>
            
            <nav class=" navbar-default hidden-xs" role="navigation">
                <ul class="nav navbar-nav">
                <li><a href="nuevo.php">Nuevo inmueble</a></li>
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">Ajustes<span class="caret"></span></a>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="perfil.php">Perfil</a></li>
                    <li><a href="inc/logout.php">Salir</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
            
            <ul class="nav-toolbar">

            </ul>
        </header>
        <div class="warper container-fluid">
          <div class="page-header"><h1>Cavalia <small>Editar</small></h1></div>
          <div class="panel panel-primary">
            <div class="panel-heading">
            Editar inmueble
            </div>
            <div class="panel-body">
                <div class="row">
                    <?php 
                     $sql = sprintf("SELECT * FROM inmueble WHERE idinmueble=%d",$_GET['idinmueble']);
                      $query = $mysqli->query($sql);
                      while($row = $query->fetch_assoc()){
                          
                    ?>
                    <form id="formInmueble">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Título</label>
                            <input type="text" name="titulo" placeholder="Ingrese el título" class="form-control form-control-flat" value="<?php echo $row['titulo'];?>">
                            <input type="hidden" name="idinmueble" id="idinmueble" value="<?php echo $row['idinmueble'];?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Precio</label>
                            <input type="text" name="precio" placeholder="Precio del inmueble" class="form-control form-control-flat" value="<?php echo $row['precio'];?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Dirección</label>
                            <input type="text" name="direccion" placeholder="Dirección completa" class="form-control form-control-flat" value="<?php echo $row['direccion'];?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tipo</label>
                            <select class="form-control form-control-flat" name="tipo">
                                <option value="">Seleccione una opción</option>
                                <?php 
                                   if($row['tipo'] == 1){
                                      echo '<option value="1" selected>Departamento</option>
                                            <option value="2">Casa</option>';
                                   }else{
                                      echo '<option value="1">Departamento</option>
                                            <option value="2" selected>Casa</option>';
                                   }
                                  ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nivel</label>
                            <input type="text" name="nivel" placeholder="Nivel en el que se encuentra el inmueble" class="form-control form-control-flat" value="<?php echo $row['nivel'];?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Número de recamaras</label>
                            <input type="text" name="recamaras" placeholder="¿Cuántas recamaras tiene el inmueble?" class="form-control form-control-flat" value="<?php echo $row['recamaras'];?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Baños</label>
                            <input type="text" name="banios" placeholder="¿Cuántos baños tiene el inmueble?" class="form-control form-control-flat" value="<?php echo $row['banios'];?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Situación del inmueble</label>
                            <select class="form-control form-control-flat" name="estado">
                                <option value="">Seleccione una opción</option>
                                <?php 
                                   if($row['tipo'] == 1){
                                      echo '<option value="1" selected>Renta</option>
                                            <option value="2">Venta</option>';
                                   }else{
                                      echo '<option value="1">Renta</option>
                                            <option value="2" selected>Venta</option>';
                                   }
                                  ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">¿Cuenta con terraza?</label>
                            <select class="form-control form-control-flat" name="terraza">
                                <option value="">Seleccione una opción</option>
                                <?php 
                                   if($row['tipo'] == 'SI'){
                                      echo '<option value="SI" selected>Si</option>
                                            <option value="NO">No</option>';
                                   }else{
                                      echo '<option value="SI">Si</option>
                                            <option value="NO" selected>No</option>';
                                   }
                                  ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">¿Cuenta con área de servicio?</label>
                            <select class="form-control form-control-flat" name="aservicio">
                                <option value="">Seleccione una opción</option>
                                <?php 
                                   if($row['tipo'] == 'SI'){
                                      echo '<option value="SI" selected>Si</option>
                                            <option value="NO">No</option>';
                                   }else{
                                      echo '<option value="SI">Si</option>
                                            <option value="NO" selected>No</option>';
                                   }
                                  ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">¿Cuenta con cuarto de servicio?</label>
                            <select class="form-control form-control-flat" name="cservicio">
                                <option value="">Seleccione una opción</option>
                                <?php 
                                   if($row['tipo'] == 'SI'){
                                      echo '<option value="SI" selected>Si</option>
                                            <option value="NO">No</option>';
                                   }else{
                                      echo '<option value="SI">Si</option>
                                            <option value="NO" selected>No</option>';
                                   }
                                  ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Estacionamiento</label>
                            <input type="text" name="estacionamiento" placeholder="¿Número de espacios para estacionamientos?" class="form-control form-control-flat" value="<?php echo $row['estacionamiento'];?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Antigüedad</label>
                            <input type="text" name="antiguedad" placeholder="Antigüedad del inmueble" class="form-control form-control-flat" value="<?php echo $row['antiguedad'];?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Medidas</label>
                            <input type="text" name="medidas" placeholder="Medidas en metros cuadrados" class="form-control form-control-flat" value="<?php echo $row['medidas'];?>">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Vídeo promocional</label>
                            <textarea class="form-control form-control-flat" name="video" placeholder="Inserte código de inserción del vídeo">
                            <?php echo $row['video'];}?>
                            </textarea>
                        </div>
                    </div>
                    </form>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                      <form action="files" class="dropzone dz-clickable">
                        <div class="dz-default dz-message"><span></span></div>
                      </form>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
              <div class="row">
                <div class="col-sm-12 pull-right">
                  <a href="index.php" class="btn btn-default btn-flat" id="btnCancelar"> CANCELAR</a>
                  <button class="btn btn-primary btn-flat" id="btnGuardar"> GUARDAR</button>
                </div>
              </div>
            </div>
          </div> 
        </div>
        <footer class="container-fluid footer">
        	Copyright &copy; 2017 <a href="http://cavalia.com/" target="_blank">CAVALIA</a>
            <a href="#" class="pull-right scrollToTop"><i class="fa fa-chevron-up"></i></a>
        </footer>
        
    
    </section>

	 <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="js/underscore/underscore-min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <script src="js/globalize/globalize.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.min.js"></script>
    <script src="js/alertify/lib/alertify.min.js"></script>
    <script src="js/dropzone/dropzone.min.js"></script>
    <script src="js/ajaxupload.js"></script>
    <script src="js/custom.js" type="text/javascript"></script>
    <script src="js/func/jsEditarInmueble.js" type="text/javascript"></script>
</body>
</html>