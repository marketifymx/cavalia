<?php 

try{
	include("config.php");
	$accion = $_POST['accion'];
	switch ($accion) {
		case 'list':
			$sql = sprintf("SELECT a.imagen,b.idinmueble,b.direccion,b.nivel,b.medidas,b.recamaras,b.titulo,(CASE WHEN b.tipo=1 THEN 'RECAMARA' WHEN b.tipo=2 THEN 'CASA' END) as tipo,(CASE WHEN b.estado=1 THEN 'RENTA' WHEN b.estado=2 THEN 'VENTA' END) as estado,b.precio FROM inmueble b
                INNER JOIN imagen a ON(a.inmueble=b.idinmueble)
                WHERE estado=1
                GROUP BY b.idinmueble LIMIT %d,1",$_POST['index']);
                $query = $mysqli->query($sql);
                $linea = '';
                while($row = $query->fetch_assoc()){
	          		$linea = '<li class="move-right" data-sale="true">
						<div class="card">
							<a data-id="'.$row['idinmueble'].'" class="external">
						    	<img class="card-img-top" src="admin/uploads/'.$row['imagen'].'" alt="Card image cap">
						  	</a>
						  <div class="card-block">
							  <h4 class="card-title text-xs-center">'.$row['titulo'].'</h4>
							    <div class="container">
								    <div class="col-md-6">
									    <h4 class="card-title">Tipo:</h4>
									    <p>'.$row['tipo'].'</p>
									    <h4 class="card-title">Nivel:</h4>
									    <p>'.$row['nivel'].'</p>
									    <h4 class="card-title">Recámaras:</h4>
									    <p>'.$row['recamaras'].'</p>
								    </div>
								    <div class="col-md-6">
									    <h4 class="card-title">Dirección:</h4>
									    <p>'.$row['direccion'].'</p>
									    <h4 class="card-title">M2:</h4>
									    <p>'.$row['medidas'].'</p>
									    <h4 class="card-title">Precio:</h4>
									    <p>$'.number_format($row['precio']).'</p>
								    </div>
							    </div>
						  </div>
						</div>
					</li>';
	            }
            $salidaJson = array("Result" => true,"Contenido" => $linea);
			echo json_encode($salidaJson);
			break;
		case 'modal':
			$sql = sprintf("SELECT b.idinmueble,b.direccion,b.nivel,b.medidas,
				b.recamaras,b.titulo,(CASE WHEN b.tipo=1 THEN 'RECAMARA' WHEN b.tipo=2 THEN 'CASA' END) as tipo,
				(CASE WHEN b.estado=1 THEN 'RENTA' WHEN b.estado=2 THEN 'VENTA' END) as estado,
				b.precio,b.video,b.aservicio,b.cservicio,b.terraza,b.antiguedad,b.estacionamiento,b.banios 
				FROM inmueble b
                WHERE idinmueble=%d",$_POST['inmueble']);
                $query = $mysqli->query($sql);
                $linea = '';
                while($row = $query->fetch_assoc()){
	          		$linea = '<div class="modal-header dark-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				        <h2 class="modal-title font-weight-bold text-light" id="myModalLabel">'.$row['titulo'].'</h2>
				      </div>
				      <div class="modal-body">
					      <div class="container">
						      <div class="col-md-8">
							      <div id="main" role="main">
								      <section class="sliderb">
								      	<div class="flexsliderd">
										  <ul class="slides">';

										$sql1 = sprintf("SELECT imagen FROM imagen WHERE inmueble=%d",$_POST['inmueble']);
						                $query1 = $mysqli->query($sql1);
						                while($row1 = $query1->fetch_assoc()){
						                	$linea.='<li data-thumb="admin/uploads/'.$row1['imagen'].'">
												      <img src="admin/uploads/'.$row1['imagen'].'" />
												    </li>';
						                }  
										$linea.='</ul>
										</div>
								      </section>
								    </div>
						      </div>
						      <div class="col-md-4">
							      <div class="container">
										<div class="col-md-6">
											<h5 class="card-title">Tipo:</h5>
											<p>'.$row['tipo'].'</p>
											<h5 class="card-title">Nivel:</h5>
											<p>'.$row['nivel'].'</p>
											<h5 class="card-title">Recámaras:</h5>
											<p>'.$row['recamaras'].'</p>
											<h5 class="card-title">Baños:</h5>
											<p>'.$row['banios'].'</p>
											<h5 class="card-title">Terraza:</h5>
											<p>'.$row['terraza'].'</p>
										</div>
										<div class="col-md-6">
											<h5 class="card-title">Área de Servicio:</h5>
											<p>'.$row['aservicio'].'</p>
											<h5 class="card-title">Cuarto de Servicio:</h5>
											<p>'.$row['cservicio'].'</p>
											<h5 class="card-title">Estacionamiento:</h5>
											<p>'.$row['estacionamiento'].'</p>
											<h5 class="card-title">Antigüedad:</h5>
											<p>'.$row['antiguedad'].'</p>
											<h5 class="card-title">M2:</h5>
											<p>'.$row['medidas'].'</p>
										</div>
									</div>
							      <div class="container mt-1">
								      <div class="embed-responsive embed-responsive-16by9">
									      '.$row['video'].'
									  </div>
							      </div>
							      <div class="container mt-1">
								      <img class="img-fluid" src="img/360.jpg"/>
							      </div>
						      </div>
					      </div>
				      </div>';
	            }
            $salidaJson = array("Result" => true,"Contenido" => $linea);
			echo json_encode($salidaJson);
			break;
		case 'nuevo':
			$consulta = sprintf("INSERT INTO inmueble
								 SET titulo='%s',tipo=%d,nivel=%d,recamaras=%d,banios=%f,terraza='%s',aservicio='%s',cservicio='%s',estacionamiento=%d,antiguedad='%s',medidas='%s',video='%s',estado=%d,direccion='%s',precio=%f,fechapub=NOW(),fechaupd=NOW()",
								 $_POST['titulo'],$_POST['tipo'],$_POST['nivel'],$_POST['recamaras'],$_POST['banios'],					$_POST['terraza'],$_POST['aservicio'],$_POST['cservicio'],$_POST['estacionamiento'],$_POST['antiguedad'],$_POST['medidas'],$_POST['video'],$_POST['estado'],$_POST['direccion'],$_POST['precio']);

			// Ejecutamos el query
			$resultadoQuery = $mysqli -> query($consulta);
            /*OBTENGO EL ULTIMO REGISTRO QUE HE INSERTADO*/
            $result = $mysqli -> query("SELECT * FROM inmueble WHERE idinmueble = LAST_INSERT_ID()");
            $row = $result->fetch_assoc();
            $idinmueble = $row['idinmueble'];
			if($resultadoQuery == true){
				$result = true;
				$mensaje = "Registro exitoso.";
				$array = $_POST['arreglo'];
				$arreglo = explode(",", $array);
				if(sizeof($arreglo) > 0){
					for($i=0;$i<sizeof($arreglo);$i++){
						$consulta = sprintf("INSERT INTO imagen(imagen, inmueble) VALUES('%s',%d)",$arreglo[$i],$idinmueble);
						$res = $mysqli->query($consulta);
					}
				}
				
			}else{
				$result = false;
				$mensaje = "Ha ocurrido un error";
			}

			$salidaJson = array("Result" => $result,"Message" => $mensaje);
			echo json_encode($salidaJson);
			break;
		case "editar":
            $consulta = sprintf("UPDATE inmueble SET titulo='%s',tipo=%d,nivel=%d,recamaras=%d,banios=%f,terraza='%s',aservicio='%s',cservicio='%s',estacionamiento=%d,antiguedad='%s',medidas='%s',video='%s',estado=%d,direccion='%s',precio=%f,fechaupd=NOW() WHERE idinmueble=%d",
								 $_POST['titulo'],$_POST['tipo'],$_POST['nivel'],$_POST['recamaras'],$_POST['banios'],$_POST['terraza'],$_POST['aservicio'],$_POST['cservicio'],$_POST['estacionamiento'],$_POST['antiguedad'],$_POST['medidas'],$_POST['video'],$_POST['estado'],
								 $_POST['direccion'],$_POST['precio'],$_POST['idinmueble']);

			// Ejecutamos el query
			$resultadoQuery = $mysqli -> query($consulta);
			// Validamos que se haya actualizado el registro
			if($mysqli -> affected_rows == 1){
				$jTableResult = array();
				$jTableResult['Result'] = true;
				print json_encode($jTableResult);

			}else{
				$jTableResult = array();
				$jTableResult['Result'] = false;
				print json_encode($jTableResult);
			}
			break;
		case 'eliminar':
			$consulta = sprintf("DELETE FROM inmueble WHERE idinmueble=%d",$_POST['idinmueble']);

			// Ejecutamos el query
			$resultadoQuery = $mysqli -> query($consulta);
			// Validamos que se haya actualizado el registro
			if($mysqli -> affected_rows == 1){
				$jTableResult = array();
				$jTableResult['Result'] = true;
				print json_encode($jTableResult);

			}else{
				$jTableResult = array();
				$jTableResult['Result'] = false;
				print json_encode($jTableResult);
			}
			break;
		default:
			# code...
			break;
	}
}catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = false;
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>