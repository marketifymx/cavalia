<?php 
	try{
		include("config.php");

		switch ($_GET['accion']) {
			case 'list':
				$ds          = DIRECTORY_SEPARATOR; 
				$storeFolder = '../uploads';
				$consulta = sprintf("SELECT * FROM imagen WHERE inmueble=%d",$_GET['inmueble']);
				$rows = array();
				if ($result = $mysqli->query($consulta)) {
				    while ($row = $result->fetch_assoc()) {
				    	$imagen['name'] = $row['imagen'];
				    	$imagen['size'] = filesize($storeFolder.$ds.$row['imagen']);
				        $rows[] = $imagen;
				    }
				    $result->free();
				}
				// Armamos array para convertir a JSON
				echo json_encode($rows);
				break;
			default:
				# code...
				break;
		}
	}
	catch(Exception $ex){
		$jTableResult = array();
		$jTableResult['Result'] = false;
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}

?>