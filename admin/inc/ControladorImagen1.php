<?php 
	try{
		include("config.php");

		switch ($_POST['accion']) {
			case 'delete':
				$query = sprintf("DELETE FROM imagen
								 WHERE imagen='%s' AND inmueble=%d LIMIT 1",
								 $_POST['imagen'],$_POST['inmueble']);

				// Ejecutamos el query
				$resultadoQuery = $mysqli -> query($query);

				// Validamos que se haya actualizado el registro
				if($mysqli -> affected_rows == 1){
					$jTableResult = array();
					$jTableResult['Result'] = "OK";
					print json_encode($jTableResult);

				}else{
					$jTableResult = array();
					$jTableResult['Result'] = "ERROR";
					print json_encode($jTableResult);
				}
				break;
			case 'nuevo':
				$consulta = sprintf("INSERT INTO imagen SET imagen='%s',inmueble=%d",
						$_POST['imagen'],$_POST['inmueble']);

				// Ejecutamos el query
				$resultadoQuery = $mysqli -> query($consulta);
				if($resultadoQuery == true){
					$result = true;
					$mensaje = "Registro exitoso.";					
				}else{
					$result = false;
					$mensaje = "Ha ocurrido un error ".$resultadoQuery->getMessage();
				}

				$salidaJson = array("Result" => $result,"Message" => $mensaje);
				echo json_encode($salidaJson);
				break;
			default:
				# code...
				break;
		}
	}
	catch(Exception $ex){
		$jTableResult = array();
		$jTableResult['Result'] = false;
		$jTableResult['Message'] = $ex->getMessage();
		print json_encode($jTableResult);
	}

?>