<?php
try
{
	include("config.php");
	//Updating a record (updateAction)
	if($_POST["action"] == "update")
	{
		if(strlen($_POST['password']) <= 0) {
			$consulta = sprintf("UPDATE usuario
								 SET nombre='%s',email='%s',nickname='%s',imagen='%s'
								 WHERE idusuario=%d LIMIT 1",
								 $_POST['nombre'],$_POST['email'],$_POST['nickname'],$_POST['imagen'],$_POST['idusuario']);

			$resultadoQuery = $mysqli -> query($consulta);
			// Validamos que se haya actualizado el registro
			if($mysqli -> affected_rows == 1){
				$jTableResult = array();
				$jTableResult['Result'] = true;
				print json_encode($jTableResult);

			}else{
				$jTableResult = array();
				$jTableResult['Result'] = false;
				print json_encode($jTableResult);
			}
		}else{
			$consulta = sprintf("UPDATE usuario
								 SET nombre='%s',email='%s',nickname='%s',imagen='%s',password='%s'
								 WHERE idusuario=%d LIMIT 1",
								 $_POST['nombre'],$_POST['email'],$_POST['nickname'],$_POST['imagen'],$_POST['password'],$_POST['idusuario']);

			$resultadoQuery = $mysqli -> query($consulta);
			// Validamos que se haya actualizado el registro
			if($mysqli -> affected_rows == 1){
				$jTableResult = array();
				$jTableResult['Result'] = true;
				print json_encode($jTableResult);

			}else{
				$jTableResult = array();
				$jTableResult['Result'] = false;
				print json_encode($jTableResult);
			}	
		}
	}
}
catch(Exception $ex)
{
	$jTableResult = array();
	$jTableResult['Result'] = false;
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}
	
?>