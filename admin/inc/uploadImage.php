<?php
define("maxUpload", 500000);
define("maxWidth", 600);
define("maxHeight", 600);
define("uploadURL", '../uploads/');
define("fileName", 'foto_');
$ds          = DIRECTORY_SEPARATOR;
$dir= '../uploads';

require_once('php_image_magician.php');


// Tipos MIME
$fileType = array('image/jpeg','image/pjpeg','image/png');

// Bandera para procesar imagen
$pasaImgSize = false;

//bandera de error al procesar la imagen
$respuestaFile = false;

// nombre por default de la imagen a subir
$fileName = '';
// error del lado del servidor
$mensajeFile = 'ERROR EN EL SCRIPT';

// Obtenemos los datos del archivo
$tamanio = $_FILES['userfile']['size'];
$tipo = $_FILES['userfile']['type'];
$archivo = $_FILES['userfile']['name'];

// Tamaño de la imagen
$imageSize = getimagesize($_FILES['userfile']['tmp_name']);
						
// Verificamos la extensión del archivo independiente del tipo mime
$extension = explode('.',$_FILES['userfile']['name']);
$num = count($extension)-1;


// Creamos el nombre del archivo dependiendo la opción
$imgFile = fileName.time().'.'.$extension[$num];

//if($imageSize[0] < maxWidth && $imageSize[1] < maxHeight)
$pasaImgSize = true;

// Verificamos el status de las dimensiones de la imagen a publicar
if($pasaImgSize == true)
{

	// Verificamos Tamaño y extensiones
	if(in_array($tipo, $fileType) && $tamanio>0 && $tamanio<=maxUpload && ($extension[$num]=='jpg' || $extension[$num]=='png'))
	{
		// Intentamos copiar el archivo
		if(is_uploaded_file($_FILES['userfile']['tmp_name']))
		{
			if(move_uploaded_file($_FILES['userfile']['tmp_name'], uploadURL.$imgFile))
			{
				$tempFile = $_FILES['userfile']['tmp_name'];                    
      
			    $targetPath = $dir . $ds;
				
				$file = $_FILES['userfile']['name'];
				$ext = pathinfo($file, PATHINFO_EXTENSION);
				
				$fn = 'img-'.time().rand(0,10).$ext;
			     
			    $targetFile =  $targetPath. $fn;
			    //getting name from the request 

			    $magicianObj = new imageLib($targetPath. $imgFile);
			    $magicianObj -> resizeImage(30, 30);
			    $magicianObj -> saveImage($targetPath.'thumb-'.$imgFile, 100);

				$respuestaFile = 'done';
				$fileName = $imgFile;
				$mensajeFile = $imgFile;
			}
			else
				// error del lado del servidor
				$mensajeFile = 'No se pudo subir el archivo';
		}
		else
			// error del lado del servidor
			$mensajeFile = 'No se pudo subir el archivo';
	}
	else
		// Error en el tamaño y tipo de imagen
		$mensajeFile = 'Verifique el tamaño y tipo de imagen';
					
}
else
	// Error en las dimensiones de la imagen
	$mensajeFile = 'Verifique las dimensiones de la Imagen';

	$salidaJson = array("respuesta" => $respuestaFile,
					"mensaje" => $mensajeFile,
					"fileName" => $fileName);

echo json_encode($salidaJson);
?>