<?php 
  session_start();
  //Comprueba la sesion
  if(!isset($_SESSION['usr_id'])){
     header("location:login.html");
  }

  include("inc/config.php");
?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cavalia - Backend</title>
	<meta name="description" content="">
	<meta name="author" content="Danny Garcia">
	<link rel="stylesheet" href="css/bootstrap/bootstrap.css" /> 
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.8.0/css/alertify.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.8.0/css/themes/default.min.css">
  <link rel="stylesheet" href="css/app.v1.css" />
  <link rel="stylesheet" href="css/style.css">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>	
	<!-- Loader -->
    <div class="loading-container">
      <div class="loading">
        <div class="l1">
          <div></div>
        </div>
        <div class="l2">
          <div></div>
        </div>
        <div class="l3">
          <div></div>
        </div>
        <div class="l4">
          <div></div>
        </div>
      </div>
    </div>
	<aside class="left-panel">
            <div class="user text-center">
                  <img src="uploads/<?php echo $_SESSION['usr_img'];?>" class="img-circle" alt="...">
                  <h4 class="user-name"><?php echo $_SESSION['usr_nombre'];?></h4>
                  
                  <div class="dropdown user-login">
                  <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-circle status-icon available"></i> Disponible <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation"><a role="menuitem" href="inc/logout.php"><i class="fa fa-circle status-icon signout"></i> Salir</a></li>
                  </ul>
                  </div>	 
            </div>
            
            <nav class="navigation">
            	<ul class="list-unstyled">
                  <li><a href="inmuebles.php"><i class="fa fa-home"></i><span class="nav-label">Inmuebles</span></a></li>
                  <li><a href="inc/logout.php"><i class="fa fa-power-off"></i><span class="nav-label">Salir</span></a></li>
                </ul>
            </nav>
            
    </aside>
    
    <section class="content">
    	
        <header class="top-head container-fluid">
            <button type="button" class="navbar-toggle pull-left">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <form role="search" class="navbar-left app-search pull-left hidden-xs">
              <input type="text" placeholder="Buscar..." class="form-control form-control-circle">
         	</form>
            
            <nav class=" navbar-default hidden-xs" role="navigation">
                <ul class="nav navbar-nav">
                <li><a href="nuevo.php">Nuevo inmueble</a></li>
                <li class="dropdown">
                  <a data-toggle="dropdown" class="dropdown-toggle" href="#">Ajustes<span class="caret"></span></a>
                  <ul role="menu" class="dropdown-menu">
                    <li><a href="perfil.php">Perfil</a></li>
                    <li><a href="inc/logout.php">Salir</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
            
            <ul class="nav-toolbar">

            </ul>
        </header>
        <div class="warper container-fluid">
          <div class="page-header"><h1>Cavalia <small>Inmuebles</small></h1></div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-primary">
                <div class="panel-heading">
                  Todos los inmuebles
                </div>
                <div class="panel-body">
                  <table id="inmuebles">
                    <thead>
                      <th></th>
                      <th>Título</th>
                      <th>Tipo</th>
                      <th>Estado</th>
                      <th>Precio</th>
                      <th></th>
                    </thead>
                    <tbody>
                      <?php 
                       $sql = sprintf("SELECT a.imagen,b.idinmueble,b.titulo,(CASE WHEN b.tipo=1 THEN 'RECAMARA' WHEN b.tipo=2 THEN 'CASA' END) as tipo,(CASE WHEN b.estado=1 THEN 'RENTA' WHEN b.estado=2 THEN 'VENTA' END) as estado,b.precio FROM inmueble b
                        INNER JOIN imagen a ON(a.inmueble=b.idinmueble)
                        GROUP BY b.idinmueble");
                        $query = $mysqli->query($sql);
                        while($row = $query->fetch_assoc()){
                          echo '<tr id="tr'.$row['idinmueble'].'">
                                <td><img src="uploads/'.$row['imagen'].'" class="img-responsive img-thumbnail img-list text-center"></td>
                                <td>'.$row['titulo'].'</td>
                                <td>'.$row['tipo'].'</td>
                                <td>'.$row['estado'].'</td>
                                <td>'.$row['precio'].'</td>
                                <td><a href="editarinmueble.php?idinmueble='.$row['idinmueble'].'" class="btn btn-primary"><i class="fa fa-edit"></i></a> <a data-id="'.$row['idinmueble'].'" class="btn btn-danger external"><i class="fa fa-trash-o"></i></a></td>
                              </tr>';
                        }
                      ?>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>     
        </div>
        <footer class="container-fluid footer">
        	Copyright &copy; 2017 <a href="http://cavalia.com/" target="_blank">CAVALIA</a>
            <a href="#" class="pull-right scrollToTop"><i class="fa fa-chevron-up"></i></a>
        </footer>
        
    
    </section>

	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
  <script src="js/underscore/underscore-min.js"></script>
  <script src="js/bootstrap/bootstrap.min.js"></script>
  <script src="js/globalize/globalize.min.js"></script>
  <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
  <script type="text/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.8.0/alertify.min.js"></script>
  <script src="js/custom.js" type="text/javascript"></script>
  <script type="text/javascript">
    $(function(){
      $("#inmuebles").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
        }
      });

      $(".external").on('click', function(e){
        e.preventDefault();
        var id = $(this).attr("data-id");
        alertify.confirm('Eliminar inmueble', '¿Estás seguro de eliminar este inmueble?', function(){ 
            //Click en OK 
            $.ajax({
                beforeSend: function(){
                    alertify.message("Espera..");
                },
                cache: false,
                type: "POST",
                dataType: "json",
                url:"inc/ControladorInmueble.php",
                data:"accion=eliminar&idinmueble="+id,
                success: function(response){
                    if(response.Result == false){
                        alertify.error("Error al eliminar el inmueble");
                    }else{
                        alertify.success("Se ha eliminado correctamente");
                        $("#tr"+id).remove();
                    }
                },
                error:function(){
                    alert('ERROR GENERAL DEL SISTEMA, INTENTE MAS TARDE');
                }
            });
          }, function(){ 
            //Click en Cancelar
          }
        );

      });
    });
  </script>
</body>
</html>