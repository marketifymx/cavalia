<?php include("admin/inc/config.php"); ?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Cavalia – Departamentos y casas en venta en Guadalajara</title>
<meta name="description" content="Tenemos los departamentos y casas en venta en Guadalajara ideales para ti y tu familia.">
<meta name="keywords" content="Casas en venta en Guadalajara, Cavalia, departamentos en venta Guadalajara, fraccionamientos Zapopan">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
<link rel="stylesheet" href="js/flexslider/flexslider.css">
<link rel="stylesheet" href="js/fancybox/jquery.fancybox.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<!-- Minimal Stylesheet -->
<link href="css/style.css" rel="stylesheet"/>
<!-- Font Awesome -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="shortcut icon" href="img/favicon.ico">
</head>
<body>
		<nav class="navbar navbar-fixed-top navbar-light transparent text-xs-right">
			<a class="navbar-brand" href="#">Cavalia</a>
			<button class="navbar-toggler hidden-lg-up dark-gray text-light" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">Menú</button>
		  	<div class="collapse navbar-toggleable-md" id="navbarResponsive">
		    <div class="col-md-8 text-xs-right">
					<ul class="nav navbar-nav float-md-right">
					  	<li class="nav-item">
					    	<a class="nav-link" href="index.php">Home</a>
					    </li>
					    <li class="nav-item">
					    	<a class="nav-link" href="desarrollos.php">Inmobiliaria</a>
					    </li>
					    <li class="nav-item">
					    	<a class="nav-link" href="index.php#desarrollos">Desarrollos y Proyectos</a>
					    </li>
					    <li class="nav-item">
					    	<a class="nav-link" href="index.php#contacto">Contacto</a>
					    </li>
					</ul>
				</div>
		    <form class="search-form form-inline text-xs-right float-md-right">
			   	<input class="search" type="text" placeholder="Buscar">
				<button class="btn btn-sm btn-search dark-gray" type="submit">
					<i class="fa fa-search text-light"></i>
				</button>
			</form>
		  </div>
		</nav>
			
	<div class="container-fluid p-0 mb-3 mt-3">
		<div class="light-header">
			<h2 class="heading">Renta</h2>
		</div>
		<div class="container inmuebles">
			<ul class="cd-gallery no-padding">
				<li>
					<ul class="cd-item-wrapper no-padding" id="listInmuebles">
						<?php 
	                       $sql = sprintf("SELECT a.imagen,b.idinmueble,b.direccion,b.nivel,b.medidas,b.recamaras,b.titulo,(CASE WHEN b.tipo=1 THEN 'DEPARTAMENTO' WHEN b.tipo=2 THEN 'CASA' END) as tipo,(CASE WHEN b.estado=1 THEN 'RENTA' WHEN b.estado=2 THEN 'VENTA' END) as estado,b.precio FROM inmueble b
	                        INNER JOIN imagen a ON(a.inmueble=b.idinmueble)
	                        WHERE estado=1
	                        GROUP BY b.idinmueble LIMIT 3");
	                        $query = $mysqli->query($sql);
	                        $i=1;
	                        while($row = $query->fetch_assoc()){
	                        	if($i == 1){
	                        		echo '<li class="move-left" ' ;
	                        	}else if($i == 2){
	                        		echo '<li class="selected" ' ;
	                        	}else if($i == 3){
	                        		echo '<li class="move-right" ' ;
	                        	}

                          		echo 'data-sale="true">
									<div class="card">
										<a data-id="'.$row['idinmueble'].'" class="external">
									  		<img class="card-img-top" src="admin/uploads/'.$row['imagen'].'" alt="Card image cap">
									  	</a>
									  <div class="card-block">
										  <h4 class="card-title text-xs-center">'.$row['titulo'].'</h4>
										    <div class="container">
											    <div class="col-md-6">
												    <h4 class="card-title">Tipo:</h4>
												    <p>'.$row['tipo'].'</p>
												    <h4 class="card-title">Nivel:</h4>
												    <p>'.$row['nivel'].'</p>
												    <h4 class="card-title">Recámaras:</h4>
												    <p>'.$row['recamaras'].'</p>
											    </div>
											    <div class="col-md-6">
												    <h4 class="card-title">Dirección:</h4>
												    <p>'.$row['direccion'].'</p>
												    <h4 class="card-title">M2:</h4>
												    <p>'.$row['medidas'].'</p>
												    <h4 class="card-title">Precio:</h4>
												    <p>$'.number_format($row['precio']).' USD</p>
											    </div>
										    </div>
									  </div>
									</div>
								</li>';
								$i++;
                        }
                      ?>
					</ul>
				</li>
			</ul>
			<button id="next" class="btn degradado"><i class="fa fa-angle-right"></i></button>
			<button id="prev" class="btn degradado"><i class="fa fa-angle-left"></i></button>
		</div>
		<div class="light-header mt-3">
			<h2 class="heading">Venta</h2>
		</div>
		<div class="container inmuebles">
			<div id="slider-down" class="flexslider">
			  <ul class="slides">
			  	<?php 
	               $sql = sprintf("SELECT a.imagen,b.idinmueble,b.direccion,b.nivel,b.medidas,b.recamaras,b.titulo,(CASE WHEN b.tipo=1 THEN 'DEPARTAMENTO' WHEN b.tipo=2 THEN 'CASA' END) as tipo,(CASE WHEN b.estado=1 THEN 'RENTA' WHEN b.estado=2 THEN 'VENTA' END) as estado,b.precio FROM inmueble b
	                INNER JOIN imagen a ON(a.inmueble=b.idinmueble)
	                WHERE estado=2
	                GROUP BY b.idinmueble");
	                $query = $mysqli->query($sql);
	                while($row = $query->fetch_assoc()){
	                	echo '<li>
							    <a data-id="'.$row['idinmueble'].'" class="external">
							      <div class="card">
										<img class="card-img-top" src="admin/uploads/'.$row['imagen'].'" alt="Card image cap">
										<div class="card-block">
											<h4 class="card-title text-xs-center">'.$row['titulo'].'</h4>
										<div class="container">
											<div class="col-md-6">
												<h4 class="card-title">Tipo:</h4>
												<p>'.$row['tipo'].'</p>
												<h4 class="card-title">Nivel:</h4>
												<p>'.$row['nivel'].'</p>
												<h4 class="card-title">Recámaras:</h4>
												<p>'.$row['recamaras'].'</p>
											</div>
											<div class="col-md-6">
												<h4 class="card-title">Dirección:</h4>
												<p>'.$row['direccion'].'</p>
												<h4 class="card-title">M2:</h4>
												<p>'.$row['medidas'].'</p>
												<h4 class="card-title">Precio:</h4>
												<p>$'.number_format($row['precio']).' USD</p>
											</div>
										</div>
									</div>
									</div>
							    </a>
							    </li>';
	                }
	            ?>
			    <!-- items mirrored twice, total of 12 -->
			  </ul>
			</div>
		</div>
	</div>

	<a data-toggle="modal" data-target="#myModal" id="enlace"></a>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content" id="contenedorModal">
	      
	    </div>
	  </div>
	</div>
	
		
<?php include('footer.php'); ?>