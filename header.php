<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Cavalia -  Desarrollos inmobiliarios en Guadalajara</title>
<meta name="description" content="Encuentra el lugar donde siempre has querido vivir en Cavalia Country Club. Departamentos y casas en venta y renta en Guadalajara, Jalisco.">
<meta name="keywords" content="Casas en venta en Guadalajara, Cavalia, departamentos en venta Guadalajara, fraccionamientos Zapopan">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css" integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous">
<link rel="stylesheet" href="js/flexslider/flexslider.css">
<link rel="stylesheet" href="js/fancybox/jquery.fancybox.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<!-- Minimal Stylesheet -->
<link href="css/style.css" rel="stylesheet"/>
<!-- Font Awesome -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="shortcut icon" href="img/favicon.ico">
</head>
	<body>
		<nav class="navbar navbar-fixed-top navbar-light transparent text-xs-right">
			<a class="navbar-brand" href="#">Cavalia</a>
			<button class="navbar-toggler hidden-lg-up dark-gray text-light" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">Menú</button>
		  	<div class="collapse navbar-toggleable-md" id="navbarResponsive">
		    <div class="col-md-8 text-xs-right">
					<ul class="nav navbar-nav float-md-right">
					  	<li class="nav-item">
					    	<a class="nav-link" href="index.php">Home</a>
					    </li>
					    <li class="nav-item">
					    	<a class="nav-link" href="desarrollos.php">Inmobiliaria</a>
					    </li>
					    <li class="nav-item">
					    	<a class="nav-link" href="index.php#desarrollos">Desarrollos y Proyectos</a>
					    </li>
					    <li class="nav-item">
					    	<a class="nav-link" href="index.php#contacto">Contacto</a>
					    </li>
					</ul>
				</div>
		    <form class="search-form form-inline text-xs-right float-md-right">
			   	<input class="search" type="text" placeholder="Buscar">
				<button class="btn btn-sm btn-search dark-gray" type="submit">
					<i class="fa fa-search text-light"></i>
				</button>
			</form>
		  </div>
		</nav>