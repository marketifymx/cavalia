<?php 
	include('header.php'); ?>
	
	<div class="container-fluid cover">
		<h1>Cavalia</h1>
	</div>
	
	<div class="container-fluid p-0">
		
		<!-- INMOBILIARIA -->
		<div class="light-header">
			<h2 class="heading">Inmobiliaria</h2>
		</div>
		<div class="container p-2 pb-3">
			<div class="col-md-4 offset-md-1">
				<a class="renta degradado btn btn-lg" href="desarrollos.php">Renta</a>
			</div>
			<div class="col-md-4 offset-md-2">
				<a class="renta degradado btn btn-lg" href="desarrollos.php">Venta</a>
			</div>
		</div>
		
		<!-- QUIENES SOMOS -->
		<div id="quienes" class="dark-header">
			<h2 class="heading text-light">¿Quiénes Somos?</h2>
		</div>
		<div class="container p-3 no-padding-m">
			<div class="col-xs-12 col-md-10 offset-md-1">
				<p class="big">
				Cavalia desarrolla proyectos enfocados al crecimiento de la metrópoli acompañados de responsabilidad y compromiso social con su entorno.
<br/><br/>
<span class="font-italic">“Como promotores tenemos que ser sensibles a la trascendencia social de nuestra propuesta. No se trata de dejar huella a ultranza, sino de hacer las cosas con seriedad y propuestas inteligentes”.</span> -Jaime Castiello
<br/><br/>
Integrado por un grupo interdisciplinario de profesionistas enfocados en el desarrollo, construcción, mantenimiento y comercialización comprometidos en dar soluciones inmobiliarias.
				</p>
			</div>
		</div>
		
		<!-- DESARROLLO Y PROYECTOS -->
		<div id="desarrollos" class="dark-header">
			<h2 class="heading text-light">Desarrollos y Proyectos</h2>
		</div>
		<div class="container-fluid p-2 desarrollos">
			<div class="row mt-3 mb-3 text-xs-center text-md-left">
				<div class="col-md-12">
					<div class="col-md-2">
						<a href="https://pisoscavalia.com.mx" target="_blank">
							<img class="img-fluid logos" src="img/desarrollos/country.png" />
						</a>
					</div>
					<div class="col-md-7">
						<div class="col-md-7 no-padding">
							<img class="img-fluid mb-1" src="img/desarrollos/country1.jpg" />
						</div>
						<div class="col-md-5">
							<img class="img-fluid" src="img/desarrollos/country2.jpg" />
							<img class="img-fluid mt-1" src="img/desarrollos/country3.jpg" />
							<img class="img-fluid mt-1" src="img/desarrollos/country4.jpg" />
						</div>
					</div>
					<div class="col-md-3">
						<h4 class="font-weight-bold">CAVALIA COUNTRY CLUB</h4>
						<p>
El lugar en donde siempre has querido vivir.<br/>
El edificio consta de 64 unidades distribuidas en 19 niveles, todos los departamentos cuentan con terrazas para disfrutar las vistas que envuelven tanto al Country Club como las panorámicas orientadas hacia el norponiente de la ciudad.<br/>
De estilo contemporáneo, buen gusto, brindando funcionalidad en los espacios y respetando las raíces y herencia de la arquitectura tapatía.
						</p>
					</div>
				</div>
			</div>
			<div class="row mt-3 mb-3 text-xs-center text-md-left">
				<div class="col-md-12">
					<div class="col-md-2">
						<img class="img-fluid logos" src="img/desarrollos/marmara.png" />
					</div>
					<div class="col-md-7">
						<div class="col-md-7 no-padding">
							<img class="img-fluid mb-1" src="img/desarrollos/marmara1.jpg" />
						</div>
						<div class="col-md-5">
							<img class="img-fluid" src="img/desarrollos/marmara2.jpg" />
							<img class="img-fluid mt-1" src="img/desarrollos/marmara3.jpg" />
							<img class="img-fluid mt-1" src="img/desarrollos/marmara4.jpg" />
						</div>
					</div>
					<div class="col-md-3">
						<h4 class="font-weight-bold">MAR MARMARA</h4>
						<p>
El proyecto se desarrolla en  un terreno de 1,242 m2 donde se conjuntan 11 viviendas de diferentes dimensiones y distribuciones entre sí que interactúan dejando entre ver volúmenes y materiales alrededor de un patio central ubicado en la planta de ingreso.<br/>
Las fachadas del proyecto muestran materiales regionales que se identifica profundamente con el pasado local y que propone de una manera respetuosa y dignificante la necesidad de aires de renovación que exige la ciudad. 
						</p>
					</div>
				</div>
			</div>
			<div class="row mt-3 mb-3 text-xs-center text-md-left">
				<div class="col-md-12">
					<div class="col-md-2">
						<img class="img-fluid logos" src="img/desarrollos/cavalia.png" />
					</div>
					<div class="col-md-7">
						<div class="col-md-7 no-padding">
							<img class="img-fluid mb-1" src="img/desarrollos/cavalia1.jpg" />
						</div>
						<div class="col-md-5">
							<img class="img-fluid" src="img/desarrollos/cavalia2.jpg" />
							<img class="img-fluid mt-1" src="img/desarrollos/cavalia3.jpg" />
						</div>
					</div>
					<div class="col-md-3">
						<h4 class="font-weight-bold">PROYECTOS</h4>
						<p>
Nos enfocamos en dar soluciones integrales a los proyectos relacionados con la arquitectura. Esta visión ha permitido que nuestro portafolio de trabajo abarque diversas áreas: uni y plurifamiliar, comercial, diseño de interiores, construcción y gestión de proyectos.</p>
						<br/>
						<h4 class="font-weight-bold">MANTENIMIENTO</h4>
<p>El mantenimiento adecuado y oportuno es vital para el cuidado de los bienes inmuebles por eso ofrecemos la alternativa con un servicio de calidad tanto para casa habitación como comercial dando un servicio integral.
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<!-- CONTACTO -->
		<div id="contacto" class="light-header">
			<h2 class="heading">Contacto</h2>
		</div>
		<div class="container">
			<div class="col-md-5">
				<h4 class="text-uppercase"><i class="fa fa-home"></i>&nbsp;&nbsp;Dirección</h4>
				<p class="ml-3">
					Ávila Camacho 2255<br/>
					Col. Country Club 44610<br/>
					Guadalajara Jal
				</p>
				<br/><br/>
				<h4 class="text-uppercase"><i class="fa fa-phone"></i>&nbsp;&nbsp;Teléfono</h4>
				<p class="ml-3">
					(33) 3854 9310<br/>
				</p>
				<br/><br/>
				<h4 class="text-uppercase"><i class="fa fa-comment"></i>&nbsp;&nbsp;E-mail</h4>
				<p class="ml-3">
					contacto@pisoscavalia.mx
				</p>
			</div>
			<div class="col-md-7">
				<div id="map"></div>
			</div>
		</div>
		
		<!-- FORMULARIO -->
		<div class="container mt-2 mb-3">
			<h3 id="resultado" class="off p-2"></h3>
			<form id="contact" method="post">
				<div class="row">
						<div class="col-md-4">
							<input id="nombre" class="form-control" type="text" name="nombre" placeholder="NOMBRE" required />
						</div>
						<div class="col-md-4">
							<input id="tel" class="form-control" type="text" name="tel" placeholder="TELÉFONO" pattern="\d*" maxlength="10" minlength="10" onkeypress="return isNumberKey(event)" required />
						</div>
						<div class="col-md-4">
							<input id="email" class="form-control" type="text" name="email" placeholder="CORREO" required />
						</div>
					</div>
					<div class="row pl-1 pr-1">
						<textarea id="mensaje" class="form-control" name="mensaje" placeholder="MENSAJE" rows="7"></textarea>
					</div>
					<div class="row mt-1 pl-1 pr-1 text-md-right">
						<button id="submit" class="btn degradado text-light text-uppercase" type="submit">Enviar</button>
				</div>
			</form>
		</div>
	</div>
			
<?php include('footer.php'); ?>