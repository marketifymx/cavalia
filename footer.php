		</div> <!-- End of Principal Container -->

		<footer class="mt-3">
			<div class="container-fluid ml-3 mr-3 dark-gray">
				<div class="col-md-10 offset-xs-1 p-2">
					<div class="row">
						<div class="col-md-4 mb-3">
							<div class="row">
								<h5 class="text-uppercase">Contacto</h5>
							</div>
							<p>
								Ávila Camacho 2255<br/>
								Col. Country Club 44610<br/>
								Guadalajara Jal.
							</p>
						</div>
						<div class="col-md-4">
							<h5 class="text-uppercase">Teléfono</h5>
							<a href="tel:38549310"><p class="text-light m-0">38549310</p></a>
						</div>
						<div class="col-md-4 text-md-left mb-3">
							<div class="row">
								<h5 class="text-uppercase">Secciones</h5>
							</div>
							<ul class="p-0">
								<li><a href="index.php"><p class="text-uppercase text-light">Home</p></a></li>
								<li><a href="index.php#quienes"><p class="text-uppercase text-light">¿Quiénes Somos?</p></a></li>
								<li><a href="index.php#desarrollos"><p class="text-uppercase text-light">Desarrollos y Proyectos</p></a></li>
								<li><a href="desarrollos.php"><p class="text-uppercase text-light">Inmobiliaria</p></a></li>
								<li><a href="index.php#contacto"><p class="text-uppercase text-light">Contacto</p></a></li>
							</ul>
						</div>
					</div>
					<div class="row text-md-center">
						<small>Copyright © Cavalia 2017</small>
					</div>
				</div>
			</div>
		</footer>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="admin/js/jquery-1.9.1.min.js"></script>
		<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
		<script type="text/javascript" src="js/jquery.flexslider.js"></script>
		<script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
		<script src="js/main.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDw0NqzW5bep3gMWuM4uJwh7BkROiIPX6k&callback=initMap">
    </script>
		<script src="js/carousel.js"></script>
		<script type="text/javascript">
			$(function(){

					$('#myModal').on('show.bs.modal', function (e) {
						initFlexSliders(); //Inicializa cuando se abre el modal
					})

					var initFlexSliders = function() {
				      $('.flexsliderd').flexslider({
				          animation: "slide",
				          slideshow: false,
				          controlNav: "thumbnails",
				          directionNav: false, 
				          start: function(slider){
				              $('.flexsliderd').resize();
				          }
				      });
				  };
			});
		    $(window).load(function(){
		    	var initFlexSliders = function() {
				      $('.flexsliderd').flexslider({
				          animation: "slide",
				          slideshow: false,
				          controlNav: "thumbnails",
				          directionNav: false, 
				          start: function(slider){
				              $('.flexsliderd').resize();
				          }
				      });
				  };

		    	$(".external").on('click', function(e){
		    		e.preventDefault();
		    		var id = $(this).attr("data-id");
		    		$.ajax({
		                beforeSend: function(){

		                },
		                cache: false,
		                type: "POST",
		                dataType: "json",
		                url:"admin/inc/ControladorInmueble.php",
		                data:"accion=modal&inmueble="+id,
		                success: function(response){
		                	$("#contenedorModal").html(response.Contenido);
		                	$("#enlace").click();
		                	initFlexSliders();
		                },
		                error:function(){
		                    alert('ERROR GENERAL DEL SISTEMA, INTENTE MAS TARDE');
		                }
		            });
		    	});
		    	// show FlexSlider on page load
					
		    	/*$('#carousel1').flexslider({
					        animation: "slide",
					        controlNav: false,
					        directionNav: false,
					        animationLoop: false,
					        slideshow: false,
					        itemWidth: 270,
					        itemMargin: 5,
					        asNavFor: '#slider1'
					      });
					
					      $('#slider1').flexslider({
					        animation: "slide",
					        controlNav: false,
					        directionNav: false,
					        animationLoop: false,
					        slideshow: false,
					        sync: "#carousel1"
					      });*/

		      
		      $('.flexslider').flexslider({
			    animation: "slide",
			    animationLoop: true,
			    itemWidth:350,
			    itemMargin: 20,
			    controlNav: false,
			    directionNav: true,
			    prevText: '',
			    nextText: '',
			  });
		    });
		</script>
	</body>
</html>